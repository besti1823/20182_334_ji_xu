package Expr7_one;

import java.util.Scanner;

public class Test1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Comparable a [] = new Comparable[10];
        for(int i =0 ; i < 10 ; i++)
        {
            int ii = i+1;
            System.out.println("第" + ii + "个数据：");
            a[i] = in.nextInt();
        }

        //a[0] = "";
        System.out.println("正序查找，查找20182334，结果："+ Searching.linearSearch(a,20182334));
        System.out.println("倒序查找，查找20182334，结果：" + reveSearching.linearSearch(a,20182334));
        System.out.println("边界查找，查找2334，结果：" + Searching.linearSearch(a,2334));
        System.out.println("正常查找，查找34,结果：" + Searching.linearSearch(a,34));
        System.out.println("边界查找，查找5656，结果：" + Searching.linearSearch(a,5656));
        System.out.println("异常，查找1111，结果：" + Searching.linearSearch(a,1111));
        System.out.println("异常，查找489，结果：" + Searching.linearSearch(a,489));
        System.out.println("倒序查找，查找6565，结果：" + Searching.linearSearch(a,6565));
        System.out.println("正序查找，查找9898，结果：" + Searching.linearSearch(a,9898));
        System.out.println("正常查找，查找2018，结果：" + Searching.linearSearch(a,2018));
        System.out.println("异常，查找7777，结果：" + Searching.linearSearch(a,7777));
        System.out.println("正常查找，查找0000，结果：" + Searching.linearSearch(a,0000));

    }
}
