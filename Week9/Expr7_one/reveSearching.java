package Expr7_one;

public class reveSearching {
    public static Comparable linearSearch (Comparable[] data,
                                           Comparable target)
    {
        Comparable result = null;
        int index = data.length-1;

        while (result == null && index >= 0)
        {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index--;
        }

        return result;
    }
}
