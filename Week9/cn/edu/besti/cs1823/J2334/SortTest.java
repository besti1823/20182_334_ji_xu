package cn.edu.besti.cs1823.J2334;

import static cn.edu.besti.cs1823.J2334.Sorting.sort;

public class SortTest {
    public static void main(String[] args) {
        int[] ins = {2, 3, 5, 1, 23, 6, 78, 34, 23, 4, 5, 78, 34, 65, 32, 65, 76, 32, 76, 1, 9,2334,20182334};
        int[] ins2 = sort(ins);
        System.out.println("希尔排序： ");
        for (int in : ins2) {
            System.out.print(in + " ");
        }

    }

}
