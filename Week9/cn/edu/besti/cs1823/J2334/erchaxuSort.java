package cn.edu.besti.cs1823.J2334;

import List.List;

import java.util.ArrayList;
 class erchashuSort {

    private Node root;

    public void add(int data) {
        if(root==null) {
            root = new Node(data);
        }else {
            root.addNode(data);
        }
    }

    public void print() {
        root.printNode();
    }
    private class Node{
        private int data;
        private Node left;
        private Node right;

        public Node(int data) {
            this.data = data;
        }

        public void addNode(int data) {
            if(this.data>data) {
                if(this.left==null) {
                    this.left = new Node(data);
                }else {
                    this.left.addNode(data); //递归调用添加方法
                }
            }else{
                if(this.right == null) {
                    this.right = new Node(data);
                }else {
                    this.right.addNode(data);  //递归调用添加方法
                }
            }
        }
        public void printNode() {
            if(this.left!=null) {
                this.left.printNode();
            }
            System.out.print(data+" ");  //由于每一个节点都可看做是根节点，此处可直接输出自己
            if(this.right!=null) {
                this.right.printNode();
            }
        }
    }
}