package cn.edu.besti.cs1823.J2334;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import static cn.edu.besti.cs1823.J2334.Searching.Search;

public class Searching {
    //线性查找
    public static Comparable linearSearch(Comparable[] data,
                                          Comparable target) {
        Comparable result = null;
        int index = 0;

        while (result == null && index < data.length) {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }

        return result;
    }

    //二分查找
    public static Comparable binarySearch(Comparable[] data,
                                          Comparable target) {
        Comparable result = null;
        int first = 0, last = data.length - 1, mid;

        while (result == null && first <= last) {
            mid = (first + last) / 2;  // determine midpoint
            if (data[mid].compareTo(target) == 0)
                result = data[mid];
            else if (data[mid].compareTo(target) > 0)
                last = mid - 1;
            else
                first = mid + 1;
        }

        return result;
    }

    public static int InsertionSearch(int a[], int value, int low, int high) {
        //int first = 0, last = a.length-1,mid;

        int mid = (low + high) / 2;
        if (a[mid] > value) {
            return InsertionSearch(a, value, low, mid - 1);
        }

        if (a[mid] < value) {
            return InsertionSearch(a, value, mid + 1, high);
        } else
            return mid + 1;

    }


    public static int Search(int aaa[], int start, int end, int key) {
        if (start > end) {
            return -1;
        } else if (aaa == null) {
            return -1;
        }
        int mid = (start + end) / 2;
        if (aaa[mid] > key) {
            return Searching.Search(aaa, start, mid - 1, key);
        }
        if (aaa[mid] < key) {
            return Searching.Search(aaa, mid + 1, end, key);
        } else return mid + 1;

    }


    public static long f(int n) {
        if (n == 1 || n == 2)
            return 1;
        else {
            return f(n - 1) + f(n - 2);
        }
    }


    public static int[] fibo() {
        int[] f = new int[20];
        f[0] = 1;
        f[1] = 1;
        for (int i = 2; i < 20; i++) {
            f[i] = f[i - 1] + f[i - 2];
        }
        return f;
    }

//    public boolean FibonacciSearch(int[] a, int n, int key)  //a为要查找的数组,n为要查找的数组长度,key为要查找的关键字
//    {
//        int low=0;
//        int high=n-1;
//
//        int[] F=new int[20];
//        fibo(F);
//
//        int k=0;
//        while(n>F[k]-1)
//            ++k;
//
//        int[] temp;//将数组a扩展到F[k]-1的长度
//        temp=new int [F[k]-1];
//        for(int j=0;j<n;j++){
//            temp[j]=a[j];
//        }
//
//        for(int i=n;i<F[k]-1;++i)
//            temp[i]=a[n-1];
//
//        while(low<=high)
//        {
//            int mid=low+F[k-1]-1;
//            if(key<temp[mid])
//            {
//                high=mid-1;
//                k-=1;
//            }
//            else if(key>temp[mid])
//            {
//                low=mid+1;
//                k-=2;
//            }
//            else
//            {
//                if(mid<n)
//                    return true; //若相等则说明mid即为查找到的位置
//                else
//                    return false; //若mid>=n则说明是扩展的数值,返回n-1
//            }
//        }
//        return false;
//    }

    public static int Fibo(int[] a, int key) {
        int low = 0;
        int high = a.length - 1;
        int k = 0; //斐波那契分割数值下标
        int mid = 0;
        int f[] = fibo(); //获得斐波那契数列
        //获得斐波那契分割数值下标
        while (high > f[k] - 1) {
            k++;
        }

        //利用Java工具类Arrays 构造新数组并指向 数组 a[]
        int[] temp = Arrays.copyOf(a, f[k]);

        //对新构造的数组进行 元素补充
        for (int i = high + 1; i < temp.length; i++) {
            temp[i] = a[high];
        }

        while (low <= high) {
            //由于前面部分有f[k-1]个元素
            mid = low + f[k - 1] - 1;
            if (key < temp[mid]) {//关键字小于切割位置元素 继续在前部分查找
                high = mid - 1;
                /*全部元素=前部元素+后部元素
                 * f[k]=f[k-1]+f[k-2]
                 * 因为前部有f[k-1]个元素,所以可以继续拆分f[k-1]=f[k-2]+f[k-3]
                 * 即在f[k-1]的前部继续查找 所以k--
                 * 即下次循环 mid=f[k-1-1]-1
                 * */
                k--;
            } else if (key > temp[mid]) {//关键字大于切个位置元素 则查找后半部分
                low = mid + 1;
                /*全部元素=前部元素+后部元素
                 * f[k]=f[k-1]+f[k-2]
                 * 因为后部有f[k-2]个元素,所以可以继续拆分f[k-2]=f[k-3]+f[k-4]
                 * 即在f[k-2]的前部继续查找 所以k-=2
                 * 即下次循环 mid=f[k-1-2]-1
                 * */
                k -= 2;
            } else {
                if (mid <= high) {
                    return mid;
                } else {
                    return high;
                }
            }
        }
        return -1;
    }

    //顺序查找
    public static int seqsearch2(int[] a, int keytype) {
        if (a.length > 0) {
            for (int i = 0; i < a.length; i++) {
                if (a[i] == keytype)   //若查找成功，则返回元素下标i
                    return i;
            }
        }
        return -1;   //若查找不成功，返回-1；
    }

    //二分查找，查找表必须有序
    public static int binarysearch2(int[] a, int keytype) {
        if (a.length > 0) {
            int low = 0, high = a.length - 1;   //查找范围的上界和下界
            while (low <= high) {
                int mid = (low + high) / 2;   //中间位置，当前比较的数据元素位置
                if (a[mid] == keytype)   //若查找成功，返回元素下标mid
                    return mid;
                else if (a[mid] < keytype)   //中间元素比给定值小
                    low = mid + 1;       //查找范围缩小到后半段
                else             //中间元素比给定值大
                    high = mid - 1;   //查找范围缩小到前半段
            }
        }
        return -1;   //查找不成功，返回-1
    }    //分块查找

    // index代表索引数组，st2代表待查找数组，keytype代表要查找的元素，m代表每块大小
    public static int blocksearch(int[] index, int[] st2, int keytype, int m) {
        int i = shunxusearch(index, keytype);    //shunxunsearch函数返回值为带查找元素在第几块
        System.out.println("在第" + i + "块");
        if (i >= 0) {
            int j = m * i;   //j为第i块的第一个元素下标
            int curlen = (i + 1) * m;
            for (; j < curlen; j++) {
                if (st2[j] == keytype)
                    return j;
            }
        }
        return -1;
    }

    public static int shunxusearch(int[] index, int keytype) {
        if (index[0] > keytype) {   //如果第一块中最大元素大于待查找元素
            return 0;    //返回0.即待查找元素在第0块
        }
        int i = 1;
        while ((index[i - 1] < keytype) && (index[i] > keytype))
            return i;
        return -1;
    }


    static class TreeNode {
        String data;
        TreeNode left;
        TreeNode right;
        TreeNode a;

        public String getData() {
            return data;
        }

        public TreeNode getLchild() {
            return left;
        }

        public TreeNode getRchild() {
            return right;
        }

        public void setNode(String data, TreeNode left2, TreeNode right2) {
            this.data = data;
            left = left2;
            right = right2;
        }

        public TreeNode() {

        }
    }


    static class Count {
        public static int count = 0;
    }


    public static class TreeTest {
        public static TreeNode createTree(TreeNode root, String[] a, int i) {

            if (i < a.length) {
                if (a[i] == "#") {
                    root = null;
                } else {
                    TreeNode left = new TreeNode();
                    TreeNode right = new TreeNode();
                    root.setNode(a[i], createTree(left, a, ++Count.count), createTree(right, a, ++Count.count));
                }
            }
            return root;
        }
    }

    public static void traverse(TreeNode root) {
        if (root != null) {
            System.out.println(root.getData());
            traverse(root.getLchild());
            traverse(root.getRchild());
            /*if(root.data == a){
                System.out.println("已查到：" + a);
            }*/
        } else {
            System.out.println("--");
        }
    }


    public class Node {

        private Object value;
        private Node leftChild;
        private Node rightChild;

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    ", leftChild=" + leftChild +
                    ", rightChild=" + rightChild +
                    '}';
        }

        public Node(String value) {
            this.value = value;
        }

        public Node(String value, Node leftChild, Node rightChild) {
            this.value = value;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public Node getLeftChild() {
            return leftChild;
        }

        public void setLeftChild(Node leftChild) {
            this.leftChild = leftChild;
        }

        public Node getRightChild() {
            return rightChild;
        }

        public void setRightChild(Node rightChild) {
            this.rightChild = rightChild;
        }
    }


    public static class HashSearch {
        // 初始化哈希表
        static int hashLength = 12;
        static int[] hashTable = new int[hashLength];

        // 原始数据
        static int[] list = new int[]{19, 14, 23, 1, 68, 20, 84, 27, 55, 11, 10, 79};

        public static void main(String[] args) throws IOException {
            //System.out.println("*******哈希查找*******");

            // 创建哈希表
            for (int i = 0; i < list.length; i++) {
                insert(hashTable, list[i]);
            }
            System.out.println("展示哈希表中的数据：" + display(hashTable));

            while (true) {
                // 哈希表查找
                System.out.print("请输入要查找的数据：");
                int data = new Scanner(System.in).nextInt();
                int result = search(hashTable, data);
                if (result == -1) {
                    System.out.println("对不起，没有找到！");
                } else {
                    System.out.println("数据的位置是：" + result);
                }
            }
        }

        /**
         * 方法：哈希表插入
         */
        public static void insert(int[] hashTable, int data) {
            // 哈希函数，除留余数法
            int hashAddress = hash(hashTable, data);

            // 如果不为0，则说明发生冲突
            while (hashTable[hashAddress] != 0) {
                // 利用 开放定址法 解决冲突
                hashAddress = (++hashAddress) % hashTable.length;
            }

            // 将待插入值存入字典中
            hashTable[hashAddress] = data;
        }

        /**
         * 方法：哈希表查找
         */
        public static int search(int[] hashTable, int data) {
            // 哈希函数，除留余数法
            int hashAddress = hash(hashTable, data);

            while (hashTable[hashAddress] != data) {
                // 利用 开放定址法 解决冲突
                hashAddress = (++hashAddress) % (hashTable.length);
                // 查找到开放单元 或者 循环回到原点，表示查找失败
                if (hashTable[hashAddress] == 0 || hashAddress == hash(hashTable, data)) {
                    return -1;
                }
            }
            // 查找成功，返回下标
            return hashAddress + 1;
        }

        /**
         * 方法：构建哈希函数（除留余数法）
         *
         * @param hashTable
         * @param data
         * @return
         */
        public static int hash(int[] hashTable, int data) {
            return data % (hashTable.length - 1);
        }

        /**
         * 方法：展示哈希表
         */
        public static String display(int[] hashTable) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i : hashTable) {
                stringBuffer = stringBuffer.append(i + " ");
            }
            return String.valueOf(stringBuffer);
        }

    }
}
