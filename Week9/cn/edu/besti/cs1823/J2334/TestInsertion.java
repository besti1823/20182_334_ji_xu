package cn.edu.besti.cs1823.J2334;

import org.w3c.dom.ls.LSOutput;

import javax.swing.tree.TreeNode;
import java.util.Scanner;

import static cn.edu.besti.cs1823.J2334.Searching.HashSearch.*;
import static cn.edu.besti.cs1823.J2334.Searching.traverse;

public class TestInsertion {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a[] = new int[10];
        int b[] = {23,55,66};
        int i;
        for (i = 0; i < 10; i++) {
            int ii = i + 1;
            System.out.println("第" + ii + "个数据：");
            a[i] = in.nextInt();
        }
        int o = Searching.Search(a,0,a.length-1,2334);
        System.out.println("插入查找: " + o);

        System.out.println("斐波那契查找：" + Searching.Fibo(a,39));
        System.out.println("插入查找：" + Searching.seqsearch2(a,2334));
        System.out.println("二分查找：" + Searching.binarysearch2(a,20182334));
        System.out.println("分块查找：" + Searching.blocksearch(b,a,2334,4));
        System.out.println("==========================================");
//        class TreeNode{
//            String data;
//            TreeNode left ;
//            TreeNode right;
//            public String getData()
//            {
//                return data;
//            }
//            public TreeNode getLchild()	{
//                return left;
//            }
//            public TreeNode getRchild()	{
//                return right;
//            }
//            public void setNode(String data, TreeNode left2, TreeNode right2){
//                this.data = data;
//                left = left2;
//                right = right2;
//            }
//            public TreeNode(){
//
//            }
//        }
//
//        String[] c = {"A","B","#","C","D","#","#","#","E","#","F","#","#"};
//        Searching.TreeNode root = new Searching.TreeNode();
////        int ii=0;
//        root = Searching.TreeTest.createTree(root,c,0);
//        if(root == null)
//        {
//            System.out.println("null");
//        }
//        System.out.println("前序：");
//        traverse(root);
//        System.out.println();
//
//        System.out.println("======================================================");
//
//        // 初始化哈希表
//        static int hashLength = 12;
//        static int[] hashTable = new int[hashLength];
//
//        // 原始数据
//        static int[] list = new int[]{19, 14, 23, 1, 68, 20, 84, 27, 55, 11, 10, 79};
//
//        // 创建哈希表
//        for (int i = 0; i < list.length; i++) {
//            insert(hashTable, list[i]);
//        }
//        System.out.println("展示哈希表中的数据：" + display(hashTable));
//
//        while (true) {
//            // 哈希表查找
//            System.out.print("请输入要查找的数据：");
//            int data = new Scanner(System.in).nextInt();
//            int result = search(hashTable, data);
//            if (result == -1) {
//                System.out.println("对不起，没有找到！");
//            } else {
//                System.out.println("数据的位置是：" + result);
//            }
//        }

    }
}