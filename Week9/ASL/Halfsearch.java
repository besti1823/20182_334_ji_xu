package ASL;

public class Halfsearch {
    public static Comparable binarySearch (Comparable[] data,
                                           Comparable target)
    {
        Comparable result = null;
        int first = 0, last = data.length-1, mid;

        while (result == null && first <= last)
        {
            mid = (first + last) / 2;  // determine midpoint
            if (data[mid].compareTo(target) == 0)
                result = data[mid];
            else
            if (data[mid].compareTo(target) > 0)
                last = mid - 1;
            else
                first = mid + 1;
        }

        return result;
    }

    public static int BySearch(int a[],int input){
        int first = 0,last = a.length-1,result =0 ;
        int mid;
        while (first <= last && result == 0){
            mid = (first + last) / 2;
            if(a[mid] == input){
                result = a[mid];
                System.out.println(mid);
            }
            if(a[mid] < input){
                first = mid + 1;
                //System.out.println("2");
            }else {
                last = mid - 1;
                //System.out.println(mid);
            }
        }
        return result;
    }
}
