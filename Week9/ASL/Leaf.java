package ASL;
public class Leaf {
    protected int data;
    public Leaf left;
    public Leaf right;
    public Leaf(int data){
        this.data=data;
        left=null;
        right=null;
    }
}
