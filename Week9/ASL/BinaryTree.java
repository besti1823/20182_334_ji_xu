package ASL;
/*
public class Binary_tree {
    private int value;//���ֵ
    protected Binary_tree left;//��
    protected Binary_tree right;//��

    public Binary_tree(int value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }



    public Binary_tree Bin(Binary_tree root, int value) {
        Binary_tree temp = root;
        while(temp != null) {
            if(temp.getValue() == value) {
                return temp;
            }
            if(value < temp.getValue()) {
                temp = temp.left;
            } else {
                temp = temp.right;
            }
        }

        return null;
    }
}
*/


public class BinaryTree {
    private Leaf root;
    public BinaryTree(){
        root=null;
    }
    public void insert(int data){
        Leaf temp=new Leaf(data);
        if(root==null)
            root=temp;
        else{
            Leaf current=root;
            Leaf parent;
            while(true){
                parent=current;
                if(data<current.data){
                    current=current.left;
                    if(current==null){
                        parent.left=temp;
                        break;
                    }
                }
                else{
                    current=current.right;
                    if(current==null){
                        parent.right=temp;
                        break;
                    }
                }
            }
        }
    }
    public void buildTree(int[] data){
        for(int i=0;i<data.length;i++){
            insert(data[i]);
        }
    }
    public boolean search(int ele){
        Leaf current=root;
        while(current!=null){
            if(current.data==ele){
                return true;
            }
            else if(current.data>ele){
                current=current.left;
            }
            else{
                current=current.right;
            }
        }
        return false;
    }
}

