package FileTest;

import java.io.*;

public class OutInputCh {
    public static void main(String[] args) throws IOException {

            //（1）文件创建（文件类实例化）
            File file = new File("C:\\Users\\Administrator\\untitled5\\week6\\FileTest", "OutInputCh.txt");

            if (!file.exists()) {
                file.createNewFile();
            }
        System.out.println("下面是用Output写入 Input读出(OutInput字符)的数据：");
            OutputStream outputStream1 = new FileOutputStream(file);
            byte[] hello = {'H', 'e', 'l', 'l', 'o', ',', 'W', 'o', 'r', 'l', 'd', '!'};
            outputStream1.write(hello);
            outputStream1.flush();//可有可无，不执行任何操作！！！

            InputStream inputStream1 = new FileInputStream(file);
            while (inputStream1.available() > 0) {
                System.out.print((char) inputStream1.read() + "  ");
            }
            inputStream1.close();
            System.out.println("\n文件读写结束：OutputStream、InputStream先写后读");
        }
    }
