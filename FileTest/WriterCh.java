package FileTest;

import java.io.*;

public class WriterCh {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\Administrator\\untitled5\\week6\\FileTest", "WriterCh.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容");
        writer2.flush();
        writer2.append("Hello,World");
        writer2.flush();

        String content = "";

        Reader reader2 = new FileReader(file);
        System.out.println("下面是用Reader读出(Writer字符输出)的数据：");
        while(reader2.ready()){
            System.out.print((char) reader2.read()+ "  ");
        }
    }
}
