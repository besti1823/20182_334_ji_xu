package FileTest;

import java.io.*;

public class BufferOutInp {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\Administrator\\untitled5\\week6\\FileTest", "BufferOutInp.txt");

        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();


        System.out.println("下面是用bufferOutput读入 bufferInput读出(OutInput字节缓存输出)的数据：");
        byte[] buffer = new byte[1024];
        String content = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag =bufferedInputStream.read(buffer))!=-1){
            content += new String(buffer,0,flag);
        }

        System.out.println(content);
        bufferedInputStream.close();
        System.out.println("文件读结束：BufferedInputStream直接读并输出！");

        //====================================BufferedOutputstream================================================
        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "利用BufferedOutputStream写入文件的缓冲区内容";
        bufferedOutputStream2.write(content2.getBytes(),0,content2.getBytes().length);
        //System.out.println(content2);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();

    }
}