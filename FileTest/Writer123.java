package FileTest;

import java.io.*;
import java.util.Scanner;

public class Writer123 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\Administrator\\untitled5\\week6\\FileTest", "Writer123.txt");

        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();
        String content = "";
        Writer writer2 = new FileWriter(file);

        Scanner b = new Scanner(System.in);
        String a = b.nextLine();
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容"+a);
        writer2.flush();
        //writer2.append("Hello,World");
        writer2.flush();


        Reader reader2 = new FileReader(file);


        BufferedReader bufferedReader = new BufferedReader(reader2);

        System.out.println("\n下面是用BufferedReader读出(writer字节缓存输出)的数据：");

        while ((content =bufferedReader.readLine())!= null){
            System.out.println(content);
        }
    }
}