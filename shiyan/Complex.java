import java.util.Objects;

public class Complex {

        private double RealPart;
        private double ImagePart;
        public Complex(double R, double I){
            RealPart = R;
            ImagePart = I;
        }

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    public Complex ComplexAdd(Complex c){
            double I = this.ImagePart + c.ImagePart;
            double R = this.RealPart + c.RealPart;
            return new Complex(R,I);
    }

    public Complex ComplexSub(Complex c){
            double I = this.ImagePart - c.ImagePart;
            double R = this.RealPart - c.RealPart;
            return new Complex(R,I);
    }

    public Complex ComplexMuIti(Complex c){
            double R = this.RealPart * c.RealPart - this.ImagePart * c.ImagePart;
            double I = this.RealPart * c.ImagePart + this.ImagePart * c.RealPart;
            return new Complex(R,I);

    }

    public Complex ComplexDiv(Complex c){
            double R = (this.RealPart * c.RealPart + this.ImagePart * c.ImagePart)/(Math.pow(c.RealPart,2) + Math.pow(c.ImagePart,2));
            double I = (this.ImagePart * c.RealPart - this.RealPart * c.ImagePart)/(Math.pow(c.RealPart,2) + Math.pow(c.ImagePart,2));
            return new Complex(R,I);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Complex)) return false;
        Complex complex = (Complex) o;
        return Double.compare(complex.RealPart, RealPart) == 0 &&
                Double.compare(complex.ImagePart, ImagePart) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(RealPart, ImagePart);
    }

    @Override
    public String toString() {
        String v = " ";
            if (ImagePart > 0)
                v = RealPart + "+" + ImagePart + 'i';
            if(ImagePart < 0)
                v = RealPart + "" + ImagePart + 'i';
            if(ImagePart == 0 )
                v = RealPart + "";
        return  v;
    }


}
