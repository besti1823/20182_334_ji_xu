import junit.framework.TestCase;

public class ComplexTest extends TestCase {
    Complex c1 = new Complex(4,5);
    Complex c2 = new Complex(6,7);
    Complex c3 = new Complex(100,-100);
    Complex c4 = new Complex(50,50);
    Complex c5 = new Complex(0,0);

    @Test
    public void testComplexAdd() throws Exception {
        //c1.ComplexAdd(c2,c3);
        assertEquals("10.0+12.0i",c1.ComplexAdd(c2).toString());
        assertEquals("106.0-93.0i",c2.ComplexAdd(c3).toString());
        assertEquals("50.0+50.0i",c5.ComplexAdd(c4).toString());
    }



    @Test
    public void testComplexSub() throws Exception {
       // c1.ComplexSub(c2,c3);
        assertEquals("0.0",c2.ComplexSub(c2).toString());
        assertEquals("-94.0+107.0i",c2.ComplexSub(c3).toString());
        assertEquals("-44.0-43.0i",c2.ComplexSub(c4).toString());
    }

    @Test
    public void testComplexMuIti() throws Exception {
        assertEquals("-13.0+84.0i",c2.ComplexMuIti(c2).toString());
    }

    @Test
    public void testComplexDiv() throws Exception {
        assertEquals("1.0",c2.ComplexDiv(c2).toString());
        assertEquals("0.13+0.01i",c2.ComplexDiv(c4).toString());
    }
}