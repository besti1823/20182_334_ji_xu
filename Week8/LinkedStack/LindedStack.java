package LinkedStack;

import ArrayStack.EmptyCollectionException;

public class LindedStack<T> implements StackADT{
    private int count;
    private LinearNode<T> top;

    public LindedStack() {
        count = 0;
        top = null;
    }

    @Override
    public void push(Object element) {
        LinearNode<T> temp = new LinearNode<T> ((T) element);
        temp.setNext(top);
        top = temp;
        count++;
    }

    @Override
    public Object pop() {
        if(isEmpty())
        {
            throw new EmptyCollectionException("Stack");
        }
        T result = top.getElement();
        top = top.getNext();
        count--;

        return result;
    }

    @Override
    public Object peek() {
        if(isEmpty())
        {
            throw new EmptyCollectionException("Stack");
        }
        return top.getElement();
    }

    @Override
    public boolean isEmpty() {
        if(count == 0)
        {
            return true;
        }
        else return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String result = "<top of stack>\n";
        LinearNode current = top;

        while(current != null)
        {
            result += current.getElement() + "\n";
            current = current.getNext();
        }

        return result + "<bottom of stack>";
    }
}
