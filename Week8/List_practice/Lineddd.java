package List_practice;

public class Lineddd {
    Node head = null;


    public static class Node{
        Node next = null;
        int data;

        public Node (int data)
        {
            this.data=data;
        }
    }


    public void add(int elem){
        Node NewNode = new Node(elem);
        if(head == null) {
            head = NewNode;
            head.next = null;
            return ;
        }
        else {
            Node temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = NewNode;
        }
    }

    public void printList(){
        Node temp = head;
        while (temp != null)
        {
            System.out.print("  " + temp.data);
            temp = temp.next;
        }
    }

    public int printList2(){
        int a = head.data;
        return a;
    }

    public int length(){
        int length = 1;
        Node temp = head;
        while(temp.next != null)
        {
            length++;
            temp = temp.next;
        }
        return length;
    }

    public boolean delete(int n){

        if(n<0 || n > length())
            return false;
        else {
            if (n == 1)
                head = head.next;
            else{
                Node q =head;
                int i = 1;
                while(q.next != null)
                {
                    if(n == i)
                    {
                        q.next= q.next.next;
                        return true;
                    }
                    q = q.next;
                    //head.next = head.next.next;
                    i++;
                }

            }
        }
        return false;
    }
/*
    public boolean delete(int index) {
        if (index < 1 || index > length()) {
            return false;
        }
        if (index == 1) {
            head = head.next;
            return true;
        }
        int i = 1;
        Node preNode = head;
        Node curNode = preNode.next;
        while (curNode != null) {
            if (i == index) {
                preNode.next = curNode.next;
                return true;
            }
            preNode = curNode;
            curNode = curNode.next;
            i++;
        }
        return false;
    }*/

    public boolean add2(int n,int aa){
        int i = 1;
        Node q = head;
        Node e = q.next;
        Node w = new Node(aa);
        if(n == 1)
        {
            w.next = q;
            head = w;
        }
        while(q.next != null)
        {
            if(n == i)
            {
                w.next = e;
                q.next = w;
                return true;
            }
            q = q.next;
            e = e.next;
            i++;
        }
        return false;
    }

    public Node sort(){
        for(int i=0;i<length()-1;i++) {
            Node temp = head;
            for(int j = 0; j<length()-1-i;j++)
            {
                if(temp.next.data < temp.data) {
                    int a = temp.data;
                    temp.data = temp.next.data;
                    temp.next.data = a;
                }
                temp = temp.next;
            }
        }
        return head;
    }

    public  Node bubbleSort1() {
        //Node temp = head;
        for (int i = 0; i <  length()-1; i++) {
            Node curNode = head;
            for (int j = 0; j < length() - 1 - i; j++) {
                if (curNode.data > curNode.next.data) {
                    int temp = curNode.data;
                    curNode.data = curNode.next.data;
                    curNode.next.data = temp;
                }
                curNode = curNode.next;
            }
        }
        return head;
    }

}