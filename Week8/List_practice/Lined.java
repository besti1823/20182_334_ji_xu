package List_practice;

public class Lined {
    Node head = null;

   static class Node{
        Node next = null;
        int data;

        public Node (int data)
        {
            this.data=data;
        }
    }

    public void add(int elem){
        Node NewNode = new Node(elem);
        if(head == null) {
            head = NewNode;
            head.next = null;
            return ;
        }
        else {
            Node temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = NewNode;
        }
    }

    public void printList(){
        Node temp = head;
        while (temp != null)
        {
            System.out.print("  " + temp.data);
            temp = temp.next;
        }
    }

    public int length(){
        int length = 1;
        Node temp = head;
        while(temp.next != null)
        {
            length++;
            temp = temp.next;
        }
        return length;
    }

    public boolean delete(Node n){

        if(n == null || n.next == null)
            return false;
        else
        {
            int temp = n.data;
            n.data = n.next.data;
            n.next.data = temp;


            n.next = n.next.next;
        }
            return true;
    }
}
