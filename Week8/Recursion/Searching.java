package Recursion;

public class Searching {
    public int Search(int aaa[], int start, int end, int key ){
        if(start>end)
        {
            return -1;
        }
        else if(aaa == null){
            return -1;
        }
        int mid = (start + end) / 2 ;
        if(aaa[mid] > key)
        {
            return Search( aaa,start,mid-1,key);
        }
        if(aaa[mid] < key)
        {
            return Search(aaa,mid+1,end,key);
        }
        else return mid+1;

    }
}
