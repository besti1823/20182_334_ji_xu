public class lianxi
{
   public static void main(String[] args)
   {
       int a = 1;
       int b = 0;
       b = a++;//等价于 b=a; a++;     
       System.out.println("a=" + a + ",b=" + b);
       System.out.println("=====华丽的分割线=====");
       a = 5;
       b = 3;
       int c = 3;
       int d = 3;
	System.out.println("a++ = "+(a++));
	System.out.println("++a = "+(++a));
	System.out.println("b-- = "+(b--));
	System.out.println("--b = "+(--b));
	System.out.println("c++ = "+(c++));
	System.out.println("++d = "+(++d));

       System.out.println("=====华丽的分割线=====");
       a = 1;
       b = 0;//重新赋值为0
       b = ++a;//等价于 a++;b = a;
       System.out.println("a=" + a + ",b=" + b);
    }
 
}
