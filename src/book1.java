public class Book 
{ 
   private String bookname; 
   private String author; 
   private String press; 
   private String copyrightdate; 
   public Book() 
   { 
   bookname = null; 
   author = null; 
   press = null; 
   copyrightdate = null; 
   } 
public String getBookname() 
{ 
return bookname; 
} 
public String getAuthor() 
{ 
return author ; 
} 
public String getPress() 
{ 
return press; 
} 
public String getCopyrightdate() 
{ 
return copyrightdate; 
} 
public void setBookname(String a) 
{ 
bookname = a; 
} 
public void setAuthor(String a) 
{ 
author = a; 
} 
public void setPress(String a) 
{ 
press = a; 
} 
public void setCopyrightdate(String a) 
{ 
copyrightdate = a; 
} 
public String toString() 
{ 
return bookname + author + press + copyrightdate; 
} 
}
