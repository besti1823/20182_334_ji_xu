import java.text.NumberFormat;
import java.util.Scanner;

public class P4_7
{
    public static void main(String[] args)
    {
        final int NUM_GAMES = 12;
        int won;
        double ratio;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number of games won (0 to "
                            + NUM_GAMES + "): ");
        won = scan.nextInt();
        while (won < 0 || won > NUM_GAMES)
        {
            System.out.print("Invalid input.Please reenter: ");
            won = scan.nextInt();
        }
        ratio = (double)won/NUM_GAMES;
        NumberFormat a = NumberFormat.getPercentInstance();

        System.out.println();
        System.out.println("Winning percentage: " + a.format(ratio));
    }
}
