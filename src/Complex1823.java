import java.util.Objects;

public class Complex1823 {
    private double RealPart;
    private double ImagePart;

    public Complex1823 ComplexAdd(Complex1823 a){
        double I = this.getImagePart()+a.getRealPart();
        double R = this.getRealPart() + a.getRealPart();
        return new Complex1823(I,R);

    }

    public Complex1823 ComplexSub(Complex1823 a){
        double I = this.getImagePart() - a.getImagePart();
        double R = this.getRealPart() - a.getRealPart();
        return new Complex1823(I,R);
    }



    public Complex1823(double realPart, double imagePart) {
        RealPart = realPart;
        ImagePart = imagePart;
    }

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Complex1823)) return false;
        Complex1823 that = (Complex1823) o;
        return Double.compare(that.RealPart, RealPart) == 0 &&
                Double.compare(that.ImagePart, ImagePart) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(RealPart, ImagePart);
    }

    @Override
    public String toString() {       return "Complex1823{" +
                "RealPart=" + RealPart +
                ", ImagePart=" + ImagePart +
                '}';

    }
}
