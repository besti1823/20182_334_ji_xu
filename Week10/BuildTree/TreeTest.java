package BuildTree;

import java.util.LinkedList;

class TreeNode{
    String data;
    TreeNode left ;
    TreeNode right;
    public String getData()
    {
        return data;
    }
    public TreeNode getLchild()	{
        return left;
    }
        public TreeNode getRchild()	{
        return right;
    }
    public void setNode(String data, TreeNode left2, TreeNode right2){
        this.data = data;
        left = left2;
        right = right2;
    }
    public TreeNode(){

    }
}


class Count{
    public static int count=0;
}


public class TreeTest{
public static TreeNode createTree(TreeNode root, String[]a, int i)	{

    if(i < a.length)
    {
        if(a[i] == "#")
        {
            root = null;
        }
        else
            {
                TreeNode left = new TreeNode();
                TreeNode right = new TreeNode();
                root.setNode(  a[i]  ,  createTree(left,a,++Count.count) ,  createTree(right,a,++Count.count)  );
            }
    }		return root;
}


public static void traverse(TreeNode root)	{
    if(root != null)
    {
        System.out.println(root.getData());
        traverse(root.getLchild());
        traverse(root.getRchild());
    }else{
        System.out.println("--");
    }
}

public static  void  preOrderTraverse(TreeNode root){
    if(root != null){
        System.out.print(root.getData());
        if(root.getLchild() != null){
            preOrderTraverse(root.getLchild());
          }
        if(root.getRchild() != null){
            preOrderTraverse(root.getRchild());
        }
    }else System.out.print("-");
}

public static void zhongxu(TreeNode root){
    if(root != null){

        if(root.getLchild() != null){
            zhongxu(root.getLchild());
        }
        System.out.print(root.getData() + " ");
        if(root.getRchild() != null){
            zhongxu(root.getRchild());
        }
    }else System.out.print("-");
}

    public static void houxu(TreeNode root){
        if(root != null){

            if(root.getLchild() != null){
                zhongxu(root.getLchild());
            }

            if(root.getRchild() != null){
                zhongxu(root.getRchild());
            }
            System.out.print(root.getData() + " ");
        }else System.out.print("-");
    }

    public static void levelOrderTraverse(TreeNode root){
    TreeNode temp = root;
    LinkedList<TreeNode> linkedList = new LinkedList();
    linkedList.add(temp);
    while(!linkedList.isEmpty()){
        temp = linkedList.poll();
        System.out.print(temp.data + " ");
        if(temp.getLchild() != null ){
            linkedList.add(temp.getLchild());
        }
        if(temp.getRchild() != null){
            linkedList.add(temp.getRchild());
        }
    }
}


public static void main(String[] args)
{
    /*Scanner in = new Scanner(System.in);
    String aa = in.nextLine();
    String[] a = aa.split("/");*/
    String[] a = {"A","B","#","C","D","#","#","#","E","#","F","#","#"};
    TreeNode root = new TreeNode();
//        int ii=0;
    root = createTree(root,a,0);
    if(root == null)
    {
        System.out.println("null");
    }
    System.out.println("前序：");
    traverse(root);
    System.out.println();
    //preOrderTraverse(root);
    System.out.println("层序：");
    levelOrderTraverse(root);
    System.out.println();

    System.out.println("中序：");
    zhongxu(root);
    System.out.println();

    System.out.println("后序：");
    houxu(root);
    System.out.println();
}

}

