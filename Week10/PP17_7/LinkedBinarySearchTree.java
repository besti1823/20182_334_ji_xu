
package javafoundations;


public class LinkedBinarySearchTree<T extends Comparable<T>>
   extends LinkedBinaryTree<T> implements BinarySearchTree<T>
{

   public LinkedBinarySearchTree()
   {
      super();
   }


   public LinkedBinarySearchTree (T element)
   {
      root = new BSTNode<T>(element);
   }

   public void add (T item)
   {
      if (root == null)
         root = new BSTNode<>(item);
      else
         ((BSTNode)root).add(item);
   }


   public BSTNode findMin() {
      if(root == null){
         return null;
      }
      else {
         BSTNode next = (BSTNode) root;
         while (next.left!= null) {

            next = (BSTNode) next.left;

         }
         System.out.println(next.left);

         return (BSTNode) next.element;
      }
   }
      /*T a = root.element;
      while (a.compareTo(root.left.element) > 0)
      {
        a = root.left.element;

      }
      return current;
   }
*/

   public BSTNode findMax() {
      if(root == null){
         return null;
      }
      else {
         BSTNode next = (BSTNode) root;
         while (next.right!= null) {

            next = (BSTNode) next.right;

         }

         return (BSTNode) next.element;
      }
   }

   //-----------------------------------------------------------------
   //  Removes and returns the element matching the specified target
   //  from this binary search tree. Throws an ElementNotFoundException
   //  if the target is not found.
   //-----------------------------------------------------------------
   public T remove (T target)
   {
      BSTNode<T> node = null;

      if (root != null)
         node = ((BSTNode)root).find(target);

      if (node == null)
         throw new ElementNotFoundException ("Remove operation failed. "
            + "No such element in tree.");

      root = ((BSTNode)root).remove(target);

      return node.getElement();
   }

}
