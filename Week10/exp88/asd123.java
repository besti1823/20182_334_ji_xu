package suff_c_mid;


import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class asd123 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Input :");
        String string = input.nextLine();
        input.close();
        String[] strs = string.split(" ");
        Set<String> set = new HashSet<>();
        set.add("+");
        set.add("-");
        set.add("*");
        set.add("/");
        set.add("(");
        set.add(")");
        Stack<String> stack = new Stack<>();
        Stack<String> operator = new Stack<>();
        for(String str : strs) {
            if(!set.contains(str)) {
                stack.push(str);
            } else {
                switch (str) {
                    case "+":
                    case "-":
                        if(!operator.isEmpty() &&
                                !operator.peek().equals("(")) {
                            stack.push(operator.pop());
                        }
                        operator.push(str);
                        break;
                    case "*":
                    case "/":
                        if(!operator.isEmpty() &&
                                (operator.peek().equals("*") ||
                                        operator.peek().equals("/"))) {
                            stack.push(operator.pop());
                        }
                        operator.push(str);
                        break;
                    case "(":
                        operator.push(str);
                        break;
                    case ")":
                        while (!operator.peek().equals("(")) {
                            stack.push(operator.pop());
                        }
                        operator.pop();
                        break;
                    default:
                        System.out.println("Error");
                        break;
                }
            }
        }
        while(!operator.isEmpty()) {
            stack.push(operator.pop());
        }
        System.out.println(stack.toString());

        String expression,again;
        int result;
        Scanner in = new Scanner(System.in);

        PostfixEvaluator evaluator = new PostfixEvaluator();
        System.out.println("Enter a valid postfix expression: ");
        expression = in.nextLine();
        result = evaluator.evaluate(expression);
        System.out.println();
        System.out.println("That expression equals " + result);
    }
}
