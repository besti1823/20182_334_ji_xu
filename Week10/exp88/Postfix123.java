package suff_c_mid;

import java.util.Scanner;

public class Postfix123 {
    public static void main(String[] args) {
        String expression,again;
        int result;
        Scanner in = new Scanner(System.in);

            PostfixEvaluator evaluator = new PostfixEvaluator();
            System.out.println("Enter a valid postfix expression: ");
            expression = in.nextLine();
            result = evaluator.evaluate(expression);
            System.out.println();
            System.out.println("That expression equals " + result);


    }
}
