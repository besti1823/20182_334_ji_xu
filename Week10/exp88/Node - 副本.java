package LInkedTree;

import javax.swing.plaf.basic.BasicSplitPaneUI;

public class Node<E> {
    public E root;
    private E data;
    private Node<E> left;
    private Node<E> right;

    public Node(E e){
        this.data = e;
    }
    Node(E data,Node<E> left,Node<E> right){
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public Node() {

    }

    public void setData(E data){
        this.data = data;
    }

    public E getData(){
        return data;
    }

    public void setLeft(Node<E> left){
        this.left = left;
    }

    public Node<E> getLeft(){
        return this.left;
    }

    public void setright(Node<E> right){
        this.right = right;
    }

    public Node<E> getright(){
        return this.right;
    }

    public E getRight() {
        return (E) this.right;
    }
}
