//********************************************************************
//  BackPainExpert.java       Java Foundations
//
//  Represents a simple expert system for back pain diagnosis.
//********************************************************************

import javafoundations.*;
import java.util.Scanner;

public class BackPainExpert
{
   private LinkedBinaryTree<String> tree;

   //-----------------------------------------------------------------
   //  Sets up the diagnosis question tree.
   //-----------------------------------------------------------------
   public BackPainExpert()
   {
      String e1 = "你想成为强者吗？";

      String e2 = "oh ， 你不应该这么颓废不是吗？";//N

      String e4 = "ok，fine，至少你该努力对吗？";//N
      String e8 = "你个垃圾滚蛋";//N
      String e9 = "孩子还有救！加油";//Y

      String e5 = "看来你内心是前进的";//Y
      String e10 = "ok fine 祝你好运";//N
      String e11 = "你会成功的，加油！";//Y

      String e3 = "你很有信心？";//Y

      String e6 = "oh？ 我能感觉到你的潜力，你有很大的潜力不是吗？";//N
      String e12 = "相信我你一定可以的";//N
      String e13 = "你会成为强者的，加油！";//Y

      String e7 = "你会成为强者的！";//Y






      LinkedBinaryTree<String> n2, n3, n4, n5, n6, n7, n8, n9,
         n10, n11, n12, n13;

      n8 = new LinkedBinaryTree<String>(e8);
      n9 = new LinkedBinaryTree<String>(e9);
      n4 = new LinkedBinaryTree<String>(e4, n8, n9);

      n10 = new LinkedBinaryTree<String>(e10);
      n11 = new LinkedBinaryTree<String>(e11);
      n5 = new LinkedBinaryTree<String>(e5, n10, n11);

      n12 = new LinkedBinaryTree<String>(e12);
      n13 = new LinkedBinaryTree<String>(e13);
      n6 = new LinkedBinaryTree<String>(e6, n12, n13);

      n7 = new LinkedBinaryTree<String>(e7);

      n2 = new LinkedBinaryTree<String>(e2, n4, n5);
      n3 = new LinkedBinaryTree<String>(e3, n6, n7);

      tree = new LinkedBinaryTree<String>(e1, n2, n3);
   }

   //-----------------------------------------------------------------
   //  Follows the diagnosis tree based on user responses.
   //-----------------------------------------------------------------
   public void diagnose()
   {
      Scanner scan = new Scanner(System.in);
      LinkedBinaryTree<String> current = tree;

      System.out.println ("加油未来的自己！");
      while (current.size() > 1)
      {
         System.out.println (current.getRootElement());
         String a = scan.nextLine();
         if (a.equalsIgnoreCase("N"))
            current = current.getLeft();
         else
            current =  current.getRight();
      }

      System.out.println (current.getRootElement());
   }
}