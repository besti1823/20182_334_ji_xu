class TreeNode{
    String data;
    BuildTree.TreeNode left ;
    BuildTree.TreeNode right;
    public String getData()
    {
        return data;
    }
    public BuildTree.TreeNode getLchild()	{
        return left;
    }
        public BuildTree.TreeNode getRchild()	{
        return right;
    }
    public void setNode(String data, BuildTree.TreeNode left2, BuildTree.TreeNode right2){
        this.data = data;
        left = left2;
        right = right2;
    }
    public TreeNode(){

    }
}


