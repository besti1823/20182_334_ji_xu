public class BinarySearchTree
{ // 二叉搜索树类
    private class Node
    { // 节点类
        int data; // 数据域
        Node right; // 右子树
        Node left; // 左子树
    }

    private Node root; // 树根节点
    public void insert(int key)
    {
        Node p = new Node(); //待插入的节点
         p.data =key;

        if(root==null)
        {
            root=p;
        }
        else
        {
            Node parent = new Node();
            Node current=root;
            while(true)
            {
                parent=current;
                if(key>current.data)
                {
                    current=current.right; // 右子树
                    if(current==null)
                    {
                        parent.right=p;
                        return;
                    }
                }
                else
                {
                    current=current.left; // 左子树
                    if(current==null)
                    {
                        parent.left=p;
                        return;
                    }
                }
            }
        }
    }
    public void preOrder(Node root)
    { // 前序遍历
        if (root != null)
        {
            System.out.print(root.data + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    public void inOrder(Node root)
    { // 中序遍历
        if (root != null)
        {
            inOrder(root.left);
            System.out.print(root.data + " ");
            inOrder(root.right);
        }
    }

    public void postOrder(Node root)
    { // 后序遍历
        if (root != null)
        {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.data + " ");
        }
    }

    public Node find(int key)
    { // 从树中按照关键值查找元素
        Node current = root;
        while (current.data != key)
        {
            if (key > current.data)
                current = current.right;
            else
                current = current.left;
            if (current == null)
                return null;
        }
        return current;
    }

    public void show(Node node)
    {    //输出节点的数据域
        if(node!=null)
            System.out.println(node.data);
        else
            System.out.println("null");
    }


    private Node getSuccessor(Node delNode)    //寻找要删除节点的中序后继结点
    {
        Node successorParent=delNode;
        Node successor=delNode;
        Node current=delNode.right;

        //用来寻找后继结点
        while(current!=null)
        {
            successorParent=successor;
            successor=current;
            current=current.left;
        }

        //如果后继结点为要删除结点的右子树的左子，需要预先调整一下要删除结点的右子树
        if(successor!=delNode.right)
        {
            successorParent.left=successor.right;
            successor.right=delNode.right;
        }
        return successor;
    }

    public boolean delete(int key) // 删除结点
    {
        Node current = root;
        Node parent = new Node();
        boolean isRightChild = true;
        while (current.data != key)
        {
            parent = current;
            if (key > current.data)
            {
                current = current.right;
                isRightChild = true;
            }
            else
            {
                current = current.left;
                isRightChild = false;
            }
            if (current == null) return false; // 没有找到要删除的结点
        }
        // 此时current就是要删除的结点,parent为其父结点
        // 要删除结点为叶子结点
        if (current.right == null && current.left == null)
        {
            if (current == root)
            {
                root = null; // 整棵树清空
            }
            else
            {
                if (isRightChild)
                    parent.right = null;
                else
                    parent.left = null;
            }
            return true;
        }
        //要删除结点有一个子结点
        else if(current.left==null)
        {
            if(current==root)
                root=current.right;
            else if(isRightChild)
                parent.right=current.right;
            else
                parent.left=current.right;
            return true;
        }
        else if(current.right==null)
        {
            if(current==root)
                root=current.left;
            else if(isRightChild)
                parent.right=current.left;
            else
                parent.left=current.left;
            return true;
        }
        //要删除结点有两个子结点
        else
        {
            Node successor=getSuccessor(current);    //找到要删除结点的后继结点

            if(current==root)
                root=successor;
            else if(isRightChild)
                parent.right=successor;
            else
                parent.left=successor;

            successor.left=current.left;
            return true;
        }
    }



    public static void main(String[] args)    // test
    {
        BinarySearchTree tree=new BinarySearchTree();

        tree.insert(39);
        tree.insert(24);
        tree.insert(64);
        tree.insert(23);
        tree.insert(30);
        tree.insert(53);
        tree.insert(60);
        tree.insert(56);
        tree.insert(20182334);


        tree.preOrder(tree.root);
        /*tree.traverse(1);
        tree.traverse(2);
        tree.traverse(3);*/
        tree.find(23);
        System.out.println();
        System.out.println("查找56");
        System.out.println("查找情况：");
        tree.show(tree.find(56));
        System.out.println();
        System.out.println("查找2334");
        System.out.println("查找情况：");
        tree.show(tree.find(2334));
        System.out.println();

        System.out.println("插入12：");
        System.out.println("中序输出：");
        tree.insert(12);
        tree.inOrder(tree.root);
        System.out.println();

        System.out.println("插入20182334");
        System.out.println("后序输出：");
        tree.insert(20182334);
        tree.postOrder(tree.root);
        System.out.println();

        System.out.println("删除元素(叶子节点)：");
        System.out.println("删除 12");
        tree.delete(12);
        tree.inOrder(tree.root);
        tree.insert(12);
        System.out.println();
        System.out.println("删除元素(有一个孩子的节点)：");
        System.out.println("删除 53");
        tree.delete(53);
        tree.inOrder(tree.root);
        tree.insert(53);
        System.out.println();
        System.out.println("删除元素(有两个孩子的节点)：");
        System.out.println("删除 39");
        tree.delete(39);
        tree.inOrder(tree.root);
        tree.insert(39);
        System.out.println();
        System.out.println("删除后以中序输出，以上删除后的数据在输出之后又重新插入");
    }
}
