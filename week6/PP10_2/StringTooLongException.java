package PP10_2;

public class StringTooLongException extends Exception{
    public StringTooLongException(String message)
    {
        super(message);
    }
}
