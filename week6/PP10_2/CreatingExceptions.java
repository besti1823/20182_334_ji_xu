package PP10_2;

import javax.swing.*;
import java.util.Scanner;

public class CreatingExceptions {
    public static void main(String[] args) throws StringTooLongException {

        Scanner a = new Scanner(System.in);
        StringTooLongException problem = new StringTooLongException("StringTooLongException.");
        String d = "";
        String b = "DONE";
        for(int c=0;;c++) {
            //String d = "";
            d = a.nextLine();
            System.out.println(d);
            if (d.length() > 20) {
                System.out.println("error");
                throw problem;
            } else if (d.equals(b)) {
                break;
            } else
                continue;
        }
        System.out.println(d);
    }
}
