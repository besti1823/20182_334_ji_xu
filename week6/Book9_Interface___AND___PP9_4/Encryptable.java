package Book9_Interface___AND___PP9_4;

public interface Encryptable {
    public String encrypt();
    public String decrypt();
}
