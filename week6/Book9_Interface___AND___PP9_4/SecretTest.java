package Book9_Interface___AND___PP9_4;

public class SecretTest {
    public static void main(String[] args) {
        Secret hush = new Secret("wil Wheaton is my hero!");
        Password poiu = new Password("jixuzuishuai!",4);
        System.out.println("明文： " + hush);

        hush.encrypt();
        System.out.println("密文： " + hush);

        hush.decrypt();
        System.out.println("解密： " + hush);

        System.out.println(poiu);
        poiu.encrypt();
        System.out.println("Password" + poiu);
    }
}
