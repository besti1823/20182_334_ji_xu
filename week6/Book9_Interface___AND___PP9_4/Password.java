package Book9_Interface___AND___PP9_4;

public class Password implements Encryptable {

    private String str;
    private int k;

    public Password(String str, int k) {
        this.str = str;
        this.k = k;

    }


    @Override
    public String encrypt() {
        String string = "";
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= 'a' && c <= 'z')//如果字符串中的某个字符是小写字母
            {
                c += k % 26;//移动key%26位
                if (c < 'a')
                    c += 26;//向左超界
                if (c > 'z')
                    c -= 26;//向右超界
            } else if (c >= 'A' && c <= 'Z')//如果字符串中的某个字符是大写字母
            {
                c += k % 26;//移动key%26位
                if (c < 'A')
                    c += 26;//向左超界
                if (c > 'Z')
                    c -= 26;//向右超界
            }
            string += c;//将解密后的字符连成字符串
        }
        System.out.println("加密前：" + str);
        System.out.println("加密后为：" + string);
        return string;


    }

    @Override
    public String decrypt() {
        return null;
    }
}