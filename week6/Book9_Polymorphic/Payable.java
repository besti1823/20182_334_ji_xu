package Book9_Polymorphic;

public interface Payable {
    public double pay();
}
