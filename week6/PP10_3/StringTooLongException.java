package PP10_3;

public class StringTooLongException extends Exception{
    StringTooLongException(String message)
    {
        super(message);
    }
}
