package Book9_Interface;

public interface Encryptable {
    public void encrypt();
    public String decrypt();
}
