package Book9_Firm__AND__PP9_2;

abstract public class StaffMember implements Payable{
    protected String name;
    protected String address;
    protected String phone;

    public StaffMember(String name, String address, String phone) {
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public abstract String rest();

    @Override
    public String toString() {
        String result = "Name: " + name + "\n";
        result += "Address: " + address + "\n";
        result += "Phone: " + phone + "\n";
        result += "rest: " + rest();
        return result;
    }

   // public abstract double pay();
}
