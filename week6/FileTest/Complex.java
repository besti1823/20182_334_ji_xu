package FileTest;

public class Complex {
    private double r;
    private double i;

    public Complex(double R, double I) {
        r = R;
        i = I;
    }

    public static double getRealPart(double r) {
        return r;
    }

    public static double getImagePart(double i) {
        return i;
    }

    public Complex ComplexAdd(Complex c) {
        return new Complex(r + c.r, i + c.i);
    }


    public String toString() {
        String s = " ";
        if (i > 0)
            s =  r + "+" + i + "i";
        if (i == 0)
            s =  r + "";
        if (i < 0)
            s = r + " " + i + "i";
        return s;
    }
}