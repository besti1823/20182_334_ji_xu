package FileTest;

import java.io.*;

public class homework {
    public static void main(String[] args) throws IOException {
        FileOutputStream fout=null;
        FileInputStream finp=null;

        //（1）文件创建（文件类实例化）
        File file = new File("C:\\Users\\Administrator\\untitled5\\week6\\FileTest", "Homework.txt");

        if (!file.exists()) {
            file.createNewFile();
        }

        InputStreamReader inputStreamReader=new InputStreamReader(System.in);
        BufferedReader bufferedReader=new BufferedReader(inputStreamReader);

        fout=new FileOutputStream(file);

        String s=bufferedReader.readLine();//从键盘输入数据

        fout.write(s.getBytes());//将数据写入到文件里

        finp=new FileInputStream(file);

//定义一个字节数组,相当于缓存
        byte bytes[]=new byte[1024];

        int n=0;//实际读取到的字节数
//循环读取
        while((n=finp.read(bytes))!=-1)
        {
//把字节转成string
            String s1=new String(bytes, 0, n);
            System.out.println(s1);//显示文件内容
        }
        //String s2 = bufferedReader.readLine();
        String[] reply = s.split("i");
        hello hi = new hello(reply[0]);
        hello hii = new hello(reply[1]);
        double a,b,c,d;
        a = hi.getrealpart();
        b = hi.getimagepart();
        c = hii.getrealpart();
        d = hii.getimagepart();
        Complex c1 =new Complex(a,b);
        Complex c2 =new Complex(c,d);
        String e;
        e = c1.ComplexAdd(c2).toString();
        System.out.println(e);
        fout.write(e.getBytes(),0,e.getBytes().length);

        fout.flush();


    }
}