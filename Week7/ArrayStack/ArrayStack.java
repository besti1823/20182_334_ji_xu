package ArrayStack;

import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;
import java.util.Objects;

public class ArrayStack <T> {
    private final int DEFAULT_CAPACITY = 100;
    private int top;
    private T[] stack;

    public ArrayStack()
    {
        top = 0 ;
        stack = (T[])(new Object[DEFAULT_CAPACITY]);
    }

    public ArrayStack(int initialCapacity)
    {
        top = 0;
        stack = (T[])(new Object[initialCapacity]);
    }

    public void push(T element) {
        if (size () == stack.length) {
            expandCapacity();
        } else {
            stack[top] = element;
            top++;
        }
    }
    private void expandCapacity()
    {
        stack = Arrays.copyOf(stack,stack.length*2);
    }

    public T pop() throws EmptyCollectionException
    {
        if(isEmpty())
        {
            throw new EmptyCollectionException("Stack");
        }
        top--;
        T result = stack[top];
        stack[top] = null;

        return result;
    }
    public T peek() throws EmptyCollectionException
    {
        if(isEmpty())
        {
            throw new EmptyCollectionException("Stack");
        }
        return stack[top - 1];


    }
    public boolean isEmpty()
    {
        return size()==0;
    }

    public int size()
    {
        return top;
    }

    @Override
    public String toString() {
        return "ArrayStack{" +
                "DEFAULT_CAPACITY=" + DEFAULT_CAPACITY +
                ", top=" + top +
                ", stack=" + Arrays.toString(stack) +
                '}';
    }
}
