package suff_c_mid;

import java.util.Scanner;
import java.util.Stack;

public class PostfixEvaluator {
    private final char ADD = '+';
    private final char SUB = '-';
    private final char MUI = '*';
    private final char DIV = '/';
    private Stack<Integer> stack;

    public PostfixEvaluator()
    {
        stack = new Stack<Integer>();
    }

    public int evaluate(String expr)
    {
        int op1,op2,result = 0;
        String a ="";
        Scanner aaa = new Scanner(expr);
        //String aaa ="7    4    -3    *    1    5    +    /    *";
        while (aaa.hasNext())
        {
            a = aaa.next();
            if(isOperator(a))
            {
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                result = evalSingleOp(a.charAt(0),op1,op2);
                stack.push(result);
            }
            else
                stack.push(Integer.parseInt(a));
        }
        
        return result;
    }
    
    private boolean isOperator(String token)
    {
        return (token.equals("+")||token.equals("-")||token.equals("*")||token.equals("/"));
    }
    
    private int evalSingleOp(char operation,int op1,int op2)
    {
        int result = 0 ;
        switch(operation)
        {
            case ADD:
                result = op1 + op2;
                break;
            case SUB:
                result = op1 - op2;
                break;
            case MUI:
                result = op1 * op2;
                break;
            case DIV:
                result = op1 / op2;
                break;
            default:
                break;
        }
        return result;
    }
}

/*
import java.util.Scanner;
import java.util.Stack;
public class PostfixEvaluator {
    private final static char ADD = '+';
    private final static char SUBTRACT = '-';
    private final static char MULTIPLY = '*';
    private final static char DIVIDE = '/';

    private Stack<Integer> stack;



    public PostfixEvaluator() {
        stack = new Stack<Integer>();
    }

    public int evaluate(String expr){
        int op1,op2, result=0;
        String token;
        Scanner parser = new Scanner(expr);

        while (parser.hasNext()){
            token = parser.next();

            if (isOperator(token)){
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                result = evaluateSingleOperator(token.charAt(0),op1,op2);
                stack.push(new Integer(result));
            }else {
                stack.push(new Integer(Integer.parseInt(token)));
            }
        }
        return result;
    }

    private boolean isOperator(String token){
        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }

    private int evaluateSingleOperator(char operator, int op1 , int op2){
        int result = 0;

        switch (operator){
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1- op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
                break;
        }

        return result;
    }
}
*/