package P5_3;


public class Coin {
    enum Face {Heads, Tails}

    public Face b = Face.Heads;
    public Face c = Face.Tails;
    private int face;
    private boolean a;

    public Coin() {
        flip();
    }

    public void flip() {
        face = (int) (Math.random() * 2);
        if (face == b.ordinal())
            a = true;
        else
            a = false;
    }

    public boolean isHeads() {
        return (a);
    }

    public String toString() {
        return (a) ? "Heads" : "Tails";
    }
}
