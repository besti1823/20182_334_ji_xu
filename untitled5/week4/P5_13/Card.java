package P5_13;

public class Card {
    private static String shape;
    private static String number;
    public static String getShape(){
        int r;
        r = (int)(Math.random()*4);
        switch (r){
            case 0:shape = "Spade";break;
            case 1:shape = "Heart";break;
            case 2:shape = "Diamond";break;
            case 3:shape = "Club";break;
            default:break;
        }
        return shape;
    }

    public static String getNumber(){
        int r;
        r = (int)(Math.random()*13);
        switch (r){
            case 0:number = "1";break;
            case 1:number = "2";break;
            case 2:number = "3";break;
            case 3:number = "4";break;
            case 4:number = "5";break;
            case 5:number = "6";break;
            case 6:number = "7";break;
            case 7:number = "8";break;
            case 8:number = "9";break;
            case 9:number = "J";break;
            case 10:number = "Q";break;
            case 11:number = "K";break;
            case 12:number = "A";break;
        }
        return number;
    }
}
