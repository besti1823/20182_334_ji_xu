package s;

public class Complex implements fushu
{
    private double r;
    private double i;

    public Complex(double R, double I) {
        r = R;
        i = I;
    }

    public Complex ComplexAdd(Complex c) {
        return new Complex(r + c.r, i + c.i);
    }
    public Complex ComplexSub(Complex c) {
        return new Complex(r - c.r, i - c.i);
    }
    public Complex ComplexMulti(Complex c) {
        return new Complex(r * c.r - i * c.i, r * c.i + i * c.r);
    }
    public Complex ComplexDiv(Complex c) {
        return new Complex((r * c.i + i * c.r)/(c.i * c.i + c.r * c.r), (i * c.i + r * c.r)/(c.i * c.i + c.r * c.r));
    }

    public String toString() {
        String s = " ";
        if (i > 0)
            s =  r + "+" + i + "i";
        if (i == 0)
            s =  r + "";
        if (i < 0)
            s = r + " " + i + "i";
        return s;
    }
    public String Compare (Complex c)
    {
        if(r>c.r) return "前者比后者大";
        else if (r<c.r) return "后者比前者大";
        else return "两个一样大";
    }
}