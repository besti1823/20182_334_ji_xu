package P8_3;

public class Reading {
    public static void main(String[] args) {
        Book a = new Book();
        Magazine b = new Magazine();
        Novel c = new Novel();
        Technical d = new Technical();
        Textbook f = new Textbook();

        System.out.println(a.Pages());
        System.out.println(a.toString());

        System.out.println(b.Pages());
        System.out.println(b.toString());

        System.out.println(c.Pages());
        System.out.println(c.toString());

        System.out.println(d.Pages());
        System.out.println(d.toString());

        System.out.println(f.Pages());
        System.out.println(f.toString());

    }
}
