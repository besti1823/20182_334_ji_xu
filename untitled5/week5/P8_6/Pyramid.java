package P8_6;

public class Pyramid extends Geometry {
    private double m;

    public Pyramid(double data) {
        super( data);
        m = data;
    }


    public double area()
    {
        double area = m*m + Math.sqrt(5)/m*m;
        return area;
    }

    public double length()
    {
        double length = 8*m;
        return length;
    }

    public double volume()
    {
        double volume = m*m*m/3;
        return volume;
    }

    @Override
    public String toString() {
        return "Pyramid{" +
                "m=" + m +
                '}';
    }
}
