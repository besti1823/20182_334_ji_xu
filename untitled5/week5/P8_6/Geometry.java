package P8_6;

public class Geometry {
    private double length;
    private double area;
    private double volume;

    private double data;

    public Geometry(double data) {
        this.data = data;
    }


    public double getData() {
        return data;
    }

    public void setData(double data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Geometry{" +
                "length=" + length +
                ", area=" + area +
                ", volume=" + volume +
                ", data=" + data +
                '}';
    }
}
