package P8_6;

import javafx.css.CssMetaData;

public class G {
    public static void main(String[] args) {
        Geometry a = new Geometry(6);
        Cube b = new Cube(6);
        Ball c = new Ball(5);
        Pyramid d = new Pyramid(6);

        b.area();
        b.length();
        b.volume();

        c.area();
        c.volume();

        d.area();
        d.length();
        d.volume();

        System.out.println(b.area());
        System.out.println(c.volume());
        System.out.println(d.length());
    }
}
