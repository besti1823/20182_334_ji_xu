package P8_6;

public class Cube extends Geometry {
    private double m;
    public Cube( double data) {
        super(data);

        m = data;
    }

    public double area()
    {
        double area = m*m*6;
        return area;
    }

    public double length()
    {
        double length = m*12;
        return length;
    }

    public double volume()
    {
        double volume = m*m*m;
        return volume;
    }
}
