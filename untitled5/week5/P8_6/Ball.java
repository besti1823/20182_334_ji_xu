package P8_6;

public class Ball extends Geometry {
    private double m;
    public Ball(double data) {
        super(data);
        m = data;
    }
    public double area()
    {
        double area = 4*Math.PI*m*m;
        return area;
    }

    public double volume()
    {
        double volume = 4/3*Math.PI*m*m;
        return volume;
    }
}
