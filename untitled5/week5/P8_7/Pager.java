package P8_7;

public class Pager extends Property {
    @Override
    public String weight() {
        return "weight:600g";
    }

    @Override
    public String price() {
        return "price:$500";
    }

    @Override
    public String power() {
        return "rapid charge";
    }

    @Override
    public String manufacturer() {
        return "ASUS";
    }
}
