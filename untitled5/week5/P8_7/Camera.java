package P8_7;

public class Camera extends Property {
    @Override
    public String weight() {
        return "500g";
    }

    @Override
    public String price() {
        return"$500";
    }

    @Override
    public String power() {
        return "fast charge";
    }

    @Override
    public String manufacturer() {
        return "sony";
    }
}
