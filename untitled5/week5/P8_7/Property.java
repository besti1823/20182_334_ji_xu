package P8_7;

public abstract class Property {
    public abstract String weight();
    public abstract String price();
    public abstract String power();
    public abstract String manufacturer();

}
