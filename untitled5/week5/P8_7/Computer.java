package P8_7;

public class Computer extends Property {
    @Override
    public String weight() {
        return "3KG";
    }

    @Override
    public String price() {
        return "$6000";
    }

    @Override
    public String power() {
        return "slow ";
    }

    @Override
    public String manufacturer() {
        return "ASUS";
    }
}
