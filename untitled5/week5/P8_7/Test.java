package P8_7;

public class Test {
    public static void main(String[] args) {
        Camera a = new Camera();
        Computer b = new Computer();
        Pager c = new Pager();
        Phone d = new Phone();

        a.power();

        System.out.println(a.manufacturer());
        System.out.println(b.power());
        System.out.println(c.price());
        System.out.println(d.weight());
    }
}
