package P8_5;

public class Age extends People {
    @Override
    public String describe() {
        return "Bigger is worse";
    }

    @Override
    public String toString() {
        return "Experience is more important!";
    }
}
