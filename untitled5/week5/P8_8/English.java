package P8_8;

public class English extends Class {

    @Override
    public String teacher() {
        return "zhangchi";
    }

    @Override
    public String time() {
        return "everyday";
    }

    @Override
    public String descrip() {
        return "interesting";
    }

    @Override
    public String department() {
        return "Network security";
    }
}
