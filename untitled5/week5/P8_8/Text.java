package P8_8;

public class Text {
    public static void main(String[] args) {
        Chinese a = new Chinese();
        English b = new English();
        Java c = new Java();
        Math d = new Math();

        a.department();
        b.descrip();
        c.teacher();
        d.time();

        System.out.println(a.department());
        System.out.println(b.descrip());
        System.out.println(c.teacher());
        System.out.println(d.time());
    }
}
