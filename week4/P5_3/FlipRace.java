package P5_3;

public class FlipRace {
    public static void main(String[] args) {
        final int GOAL = 3;
        int a = 0,b = 0;
        Coin coin1 = new Coin(),coin2 = new Coin();
        while(a<GOAL&&b<GOAL)
        {
            coin1.flip();
            coin2.flip();
            System.out.println("Coin 1: "+coin1+"\tCoin 2: "+coin2);
            a = (coin1.isHeads()) ? a+1 : 0;
            b = (coin2.isHeads()) ? b+1 : 0;
        }
        if(a < GOAL)
            System.out.println("Coin 2 Wins!");
        else if (b < GOAL)
            System.out.println("Coin 1 Wins!");
        else
            System.out.println("It's a TIE!");
    }
}

