package P5_3;

public class CountFlips {
    public static void main(String[] args) {
        final int FLIPS = 1000;
        int a = 0,b = 0;
        Coin myCoin = new Coin();
        for(int count=1;count<=FLIPS;count++)
        {
            myCoin.flip();
            if(myCoin.isHeads())
                a++;
            else
                b++;
        }
        System.out.println("Number of flips: "+FLIPS);
        System.out.println("Number of heads: "+a);
        System.out.println("Number of tails: "+b);
    }
}
