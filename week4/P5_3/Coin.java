package P5_3;


public class Coin {
    enum Face {Heads, Tails}

    public Face bnm = Face.Heads;
    public Face c = Face.Tails;
    private int face;
    private boolean eer;

    public Coin() {
        flip();
    }

    public void flip() {
        face = (int) (Math.random() * 2);
        if (face == bnm.ordinal())
            eer = true;
        else
            eer = false;
    }

    public boolean isHeads() {
        return (eer);
    }

    public String toString() {
        return (eer) ? "Heads" : "Tails";
    }
}
