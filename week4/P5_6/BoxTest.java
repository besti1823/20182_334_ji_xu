package P5_6;

public class BoxTest {
    public static void main(String[] args) {
        Box a = new Box(5.0, 6.0, 7.0, false);
        Box b = new Box(10.0, 11.0, 9.0, false);
        System.out.println("Size of the Box is:");
        System.out.println("Height:5.0");
        System.out.println("Width:6.0");
        System.out.println("Depth:7.0");
        String s = a.toString();
        System.out.println(s);
        double v = 24;
        boolean c;
        if (b.getHeight() * b.getDepth() * b.getWidth() == v) {
            c = true;
        } else
            c = b.isFull();
        if (c)
            System.out.println("The Box is full!");
        else
            System.out.println("The Box is not full!");

    }
}
