//import junit.framework.TestCase;

import com.thaiopensource.relaxng.parse.Div;

import java.util.Scanner;

public class Conumcal /*extends TestCase*/ {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int flag = 1;
        //Complex com = new Complex( double R, double I );
        do {
            System.out.println("Enter the real part of the first complex number：");
            double R1 = scan.nextDouble();//输入第一个复数的实数部分
            System.out.println("Enter the image part of the first complex number");
            double I1 = scan.nextDouble();//输入第一个复数的虚数部分

            System.out.println();

            System.out.println("Enter the real part of the second complex number");
            double R2 = scan.nextDouble();//输入第二个复数的实数部分
            System.out.println("Enter the image part of the second complex number");
            double I2 = scan.nextDouble();//输入第二个复数的虚数部分

            System.out.println("Please select an algorithm:('+' '-' '*' '/') : ");
            String s = scan.next();
            char a = s.charAt(0);

            switch (a) {
                case '+':
                    double AddR = R1 + R2;
                    double AddI = I1 + I2;
                    if (AddI == 0)
                        System.out.println("result is : " + AddR);
                    if (AddI < 0)
                        System.out.println("result is : " + AddR + "" + AddI + "i");
                    if (AddI > 0)
                        System.out.println("result is : " + AddR + "+" + AddI + "i");
                    break;
                case '-':
                    double SubR = R1 - R2;
                    double SubI = I1 - I2;
                    if (SubI == 0)
                        System.out.println("result is : " + SubR);
                    if (SubI < 0)
                        System.out.println("result is : " + SubR + "" + SubI + "i");
                    if (SubI > 0)
                        System.out.println("result is : " + SubR + "+" + SubI + "i");
                    break;
                case '*':
                    double MuItiR = R1 * R2 - I1 * I2;
                    double MuItiI = R1 * I2 + I1 * R2;
                    if (MuItiI == 0)
                        System.out.println("result is : " + MuItiR);
                    if (MuItiI < 0)
                        System.out.println("result is : " + MuItiR + "" + MuItiI + "i");
                    if (MuItiI > 0)
                        System.out.println("result is : " + MuItiR + "+" + MuItiI + "i");
                    break;
                case '/':
                    double DivR = (R1 * R2 + I1 * I2) / (Math.pow(R2, 2) + Math.pow(I2, 2));
                    double DivI = (I1 * R2 - R1 * I2) / (Math.pow(R2, 2) + Math.pow(I2, 2));
                    if (DivI == 0)
                        System.out.println("result is : " + DivR);
                    if (DivI < 0)
                        System.out.println("result is : " + DivR + "" + DivI + "i");
                    if (DivI > 0)
                        System.out.println("result is : " + DivR + "+" + DivI + "i");
                    break;
                default:
                    System.out.println("worry!break!");
            }
            System.out.println("again? (Y/N)");
            String p = scan.next();
            char q = p.charAt(0);
            if(q == 'Y'|| q == 'y'){
                flag = 1;
                System.out.println("No problem!");
            }
            else if (q == 'N'|| q =='n') {
                flag = 0;
                System.out.println("Thank you for using!");
            }
            else {
                flag = 0;
                System.out.println("error!error!error!break!");
            }
        }while (flag ==1);
    }
}
